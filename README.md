# Cleath

## Description

A DAO on a polygon which can do the following things

1. People can apply for joining the DAO
2. Admin can approve membership
3. Anyone can create a proposal
4. People can vote on proposal
5. Admin can close the voting process and declare the result
