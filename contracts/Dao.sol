// SPDX-License-Identifier: MIT
pragma solidity ^0.8.0;

contract DAO {
    struct Member {
        bool approved;
        bool exists;
    }

    struct Proposal {
        uint id;
        address creator;
        string description;
        uint yesVotes;
        uint noVotes;
        bool open;
        address[] voted;
    }

    address public admin;
    uint public numMembers;
    mapping(address => Member) public members;
    Proposal[] public proposals;
    uint public numProposals;

    event MembershipApplication(address applicant);
    event MembershipApproved(address member);
    event ProposalCreated(address creator, string description);
    event VoteCasted(address voter, uint proposalId, bool vote);
    event VotingClosed(uint proposalId, bool result);

    constructor() {
        admin = msg.sender;
        numMembers = 0;
        numProposals = 0;
    }

    function applyForMembership() public {
        require(!members[msg.sender].exists, "You are already a member");
        members[msg.sender].exists = true;
        emit MembershipApplication(msg.sender);
    }

    function approveMembership(address member) public {
        require(msg.sender == admin, "Only admin can approve membership");
        require(members[member].exists, "Member does not exist");
        require(!members[member].approved, "Member is already approved");
        members[member].approved = true;
        numMembers++;
        emit MembershipApproved(member);
    }

    function createProposal(string memory description) public {
        require(
            members[msg.sender].approved,
            "You must be an approved member to create a proposal"
        );
        proposals.push(
            Proposal(
                numProposals,
                msg.sender,
                description,
                0,
                0,
                true,
                new address[](0)
            )
        );
        numProposals++;
        emit ProposalCreated(msg.sender, description);
    }

    function castVote(uint proposalId, bool vote) public {
        require(
            members[msg.sender].approved,
            "You must be an approved member to vote"
        );
        Proposal storage p = proposals[proposalId];
        require(p.open, "Voting is closed for this proposal");
        require(
            !hasVoted(p.voted, msg.sender),
            "You have already voted on this proposal"
        );
        if (vote) {
            p.yesVotes++;
        } else {
            p.noVotes++;
        }
        p.voted.push(msg.sender);
        emit VoteCasted(msg.sender, proposalId, vote);
    }

    function closeVoting(uint proposalId) public {
        require(msg.sender == admin, "Only admin can close voting");
        Proposal storage p = proposals[proposalId];
        require(p.open, "Voting is already closed for this proposal");
        p.open = false;
        bool result = p.yesVotes > p.noVotes;
        emit VotingClosed(proposalId, result);
    }

    function hasVoted(
        address[] storage voted,
        address voter
    ) internal view returns (bool) {
        for (uint i = 0; i < voted.length; i++) {
            if (voted[i] == voter) return true;
        }
        return false;
    }
}
