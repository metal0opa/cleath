const { expect } = require("chai");
const { ethers } = require("hardhat");

describe("DAO contract", function () {
  let dao;

  beforeEach(async function () {
    const DAO = await ethers.getContractFactory("DAO");
    dao = await DAO.deploy();
    await dao.deployed();
  });

  it("allows a user to apply for membership", async function () {
    const [admin, applicant] = await ethers.getSigners();
    await dao.connect(applicant).applyForMembership();
    expect((await dao.members(applicant.address)).exists).to.equal(true);
  });

  it("allows the admin to approve membership", async function () {
    const [admin, applicant] = await ethers.getSigners();
    await dao.connect(applicant).applyForMembership();
    await dao.connect(admin).approveMembership(applicant.address);

    const membership = await dao.members(applicant.address);
    expect(membership.approved).to.equal(true);
  });

  it("does not allow non-admin to approve membership", async function () {
    const [admin, applicant, nonAdmin] = await ethers.getSigners();
    await dao.applyForMembership();
    await expect(
      dao.connect(nonAdmin).approveMembership(applicant.address)
    ).to.be.revertedWith("Only admin can approve membership");
  });

  it("allows an approved member to create a proposal", async function () {
    const [admin, member] = await ethers.getSigners();
    await dao.connect(member).applyForMembership();
    await dao.connect(admin).approveMembership(member.address);
    await dao.connect(member).createProposal("Test proposal");
    const proposal = await dao.proposals(0);

    expect(proposal.id).to.equal(0);
    expect(proposal.creator).to.equal(member.address);
    expect(proposal.description).to.equal("Test proposal");
    expect(proposal.yesVotes).to.equal(0);
    expect(proposal.noVotes).to.equal(0);
    expect(proposal.open).to.equal(true);
  });

  it("does not allow a non-approved member to create a proposal", async function () {
    const [admin, nonMember] = await ethers.getSigners();
    await expect(
      dao.connect(nonMember).createProposal("Test proposal")
    ).to.be.revertedWith("You must be an approved member to create a proposal");
  });

  it("allows an approved member to cast a vote", async function () {
    const [admin, member] = await ethers.getSigners();
    await dao.connect(member).applyForMembership();
    await dao.connect(admin).approveMembership(member.address);
    await dao.connect(member).createProposal("Test proposal");
    await dao.connect(member).castVote(0, true);

    const proposal = await dao.proposals(0);
    expect(proposal.yesVotes).to.equal(1);
  });

  it("does not allow a non-approved member to cast a vote", async function () {
    const [admin, nonMember] = await ethers.getSigners();
    await dao.connect(nonMember).applyForMembership();
    await dao.connect(admin).applyForMembership();
    await dao.connect(admin).approveMembership(admin.address);
    await dao.connect(admin).createProposal("Test proposal");
    await expect(dao.connect(nonMember).castVote(0, true)).to.be.revertedWith(
      "You must be an approved member to vote"
    );
  });

  it("does not allow a member to vote more than once", async function () {
    const [admin, member] = await ethers.getSigners();
    await dao.connect(member).applyForMembership();
    await dao.connect(admin).approveMembership(member.address);
    await dao.connect(member).createProposal("Test proposal");

    let voteCasted = await dao.connect(member).castVote(0, false);
    expect(voteCasted)
      .to.emit(dao, "VoteCasted")
      .withArgs(member.address, 0, false);

    // Try to cast another vote on the same proposal
    await expect(dao.connect(member).castVote(0, true)).to.be.revertedWith(
      "You have already voted on this proposal"
    );

    // Check that the voting is closed by the admin
    await dao.connect(admin).closeVoting(0);
    let votingClosed = await dao.proposals(0);
    expect(votingClosed.open).to.be.false;
    expect(votingClosed.yesVotes).to.equal(0);
    expect(votingClosed.noVotes).to.equal(1);

    // Check that the VotingClosed event is emitted with the correct result
    expect(voteCasted).to.emit(dao, "VotingClosed").withArgs(0, false);

    // Try to cast vote on a closed proposal
    await expect(dao.connect(member).castVote(0, true)).to.be.revertedWith(
      "Voting is closed for this proposal"
    );
  });
});
