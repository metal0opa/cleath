const { network } = require("hardhat");
const { developmentChains } = require("../helper-hardhat-config");
const { verify } = require("../utils/verify");

module.exports = async ({ getNamedAccounts, deployments }) => {
  const { deployer } = await getNamedAccounts();
  const { deploy } = deployments;
  console.log("----Deploying----");
  const Dao = await deploy("DAO", {
    from: deployer,
    log: true,
    args: [],
    waitConfimations: 1,
  });
  console.log("----Deployment Was Successful----");
  if (!developmentChains.includes(network.name)) {
    console.log("-----Verifying-----");
    verify(Dao.address, []);
    console.log("----Verification was Successful----");
  }
};

module.exports.tags = ["all", "dao"];
